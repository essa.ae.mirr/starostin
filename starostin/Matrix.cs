﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    class Matrix : IMatrix
    {
        private int[,] _values;
        private int _rows;
        private int _cols;
        private Random _rnd = new Random();
        private UsualMatrixDrawer _matrixDrawer;
        internal Matrix(int rows, int cols)
        {
            _values = new int[rows,cols];
            _rows = rows;
            _cols = cols;
            _matrixDrawer = new UsualMatrixDrawer(this);
        }

        public void FillRandom()
        {
            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _cols; j++)
                {
                    _values[i, j] = _rnd.Next(9);
                }
            }
        }

        public void SetValue(int row, int col, int value)
        {
            _values[row, col] = value;
        }

        public int GetValue(int row, int col)
        {
            return _values[row, col];
        }

        public int GetRows()
        {
            return _rows;
        }

        public int GetCols()
        {
            return _cols;
        }

        public MatrixType GeType()
        {
            return MatrixType.Dense;
        }
    }
}
