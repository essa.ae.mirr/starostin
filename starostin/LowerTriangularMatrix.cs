﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    class LowerTriangularMatrix : IMatrix
    {
        private IMatrix _m;

        public LowerTriangularMatrix(IMatrix m)
        {
            _m = m;
        }

        public void SetValue(int row, int col, int value)
        {
            if(row<=col)
            _m.SetValue(row, col, value);
            
        }

        public int GetValue(int row, int col)
        {
            if (row <= col)
                return _m.GetValue(row, col);
            else return 0;
        }

        public int GetRows()
        {
            return _m.GetRows();
        }

        public int GetCols()
        {
            return _m.GetCols();
        }

        public MatrixType GeType()
        {
            return _m.GeType();
        }

        
    }
}
