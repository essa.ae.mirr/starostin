﻿namespace starostin
{
    public interface IMatrixDrawer
    {
        void SetSystem(ITableDrawingSystem system);
        void Draw();
    }
}