﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    class MatrixDrawerBuilder
    {
        public static IMatrixDrawer GetDrawer(IMatrix m)
        {
            switch (m.GeType())
            {
                case MatrixType.Dense:
                    return new UsualMatrixDrawer(m);
                    break;
                case MatrixType.Sparse:
                    return new SparseMatrixDrawer(m);
                    break;
                    
            }
            return null;
        }
    }
}
