﻿using starostin.Commands;

namespace starostin
{
    public class TransposeMatrixComannd : Command
    {
        public TransposeMatrixComannd(ISystem s) : base(s)
        {

        }
        public override void Execute()
        {
            _s.Matrix =  new TransposedMatrix(_s.Matrix);
        }
    }
}