﻿using starostin.Commands;

namespace starostin
{
    public class SparseMatrixComannd : Command
    {
     
        private SparseMatrix templateMatrix;
        public SparseMatrixComannd(ISystem s, int rows, int cols) : base(s)
        {
           
            templateMatrix = new SparseMatrix(rows, cols);
            templateMatrix.GenerateSimple();
        }
        public override void Execute()
        {
            _s.Matrix = templateMatrix;
        }
    }
}