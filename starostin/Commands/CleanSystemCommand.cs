﻿namespace starostin.Commands
{
    public class CleanSystemCommand : Command
    {
        public CleanSystemCommand(ISystem s) : base(s)
        {
        }

        public override void Execute()
        {
            _s.Matrix = null;
            _s.Clean();
        }
    }
}