﻿using System.Collections.Generic;
using starostin.Commands;

namespace starostin
{
    public class MinorMatrixComannd : Command
    {
        private List<int> _rows;
        private List<int> _cols;
        public MinorMatrixComannd(ISystem s ,List<int> rows, List<int> cols) : base(s)
        {
            _rows = rows;
            _cols = cols;
        }
        public override void Execute()
        {
            _s.Matrix =  new MinorMatrix(_s.Matrix, _cols, _rows);
        }
    }
}