﻿using starostin.Commands;

namespace starostin
{
    public class DenseMatrixComannd : Command
    {
        
        private Matrix templateMatrix;
        public DenseMatrixComannd(ISystem s, int rows, int cols) :base(s)
        {
            templateMatrix = new Matrix(rows, cols);
            templateMatrix.FillRandom();
        }
        public override void Execute()
        {
            _s.Matrix =  templateMatrix;
        }
    }
}