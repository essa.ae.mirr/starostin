﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace starostin.Commands
{
    public interface ISystem
    {
        void Clean();
        IMatrix Matrix { get; set; }
    }
}
