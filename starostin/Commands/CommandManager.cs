﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using starostin.Commands;

namespace starostin
{
    public class CommandManager
    {
        private  Stack<ICommand> _history = new Stack<ICommand>();
        private  ISystem _system;
        private static CommandManager _instance = new CommandManager();

        private CommandManager()
        {
            

        }

        public static CommandManager GetInstance()
        {
            return _instance;
        }

        public  void Init(ISystem s)
        {
            _system = s;
            _history.Push(new CleanSystemCommand(_system));
        }

        public void Undo()
        {
            
            if (_history.Count > 1)
            {
                
                _history.Pop();
                foreach (var command in _history.Reverse())
                {
                  command.Execute();
                }

               
            }
           

        }

        

        public void AddCommand(ICommand command)
        {
            _history.Push(command);
        }
    }
}
