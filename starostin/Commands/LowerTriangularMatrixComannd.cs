﻿using starostin.Commands;

namespace starostin
{
    public class LowerTriangularMatrixComannd : Command
    {
        public LowerTriangularMatrixComannd(ISystem s) : base(s)
        {
            
        }

        public override void Execute()
        {
          _s.Matrix = new LowerTriangularMatrix(_s.Matrix);
        }
    }
}