﻿namespace starostin
{
    public interface ICommand
    {
        void Execute();
    }
}