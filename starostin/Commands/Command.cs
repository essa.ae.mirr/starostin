﻿using starostin.Commands;

namespace starostin
{
    public abstract class Command : ICommand
    {
        protected ISystem _s;
        public Command(ISystem s)
        {
            _s = s;
            Register();
        }
       

        public abstract void  Execute();
       

        private void Register()
        {
            CommandManager.GetInstance().AddCommand(this);
        }
    }
}