﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using starostin.Commands;

namespace starostin
{
    public partial class Form1 : Form, ISystem
    {
        private IMatrix _m;

        public IMatrix Matrix
        {
            get => _m;
            set
            {
                _m = value;
                DrawMatrix();
            } 
        }

        private ITableDrawingSystem _console = new ConsoleTableDrawingSystem();
        private ITableDrawingSystem _html = new HtmlTableDrawingSystem();
        private ITableDrawingSystem _graphic = new GraphicTableDrawingSystem();
        public Form1()
        {
            InitializeComponent();
        }

        private void usualMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new DenseMatrixComannd(this, 3,3).Execute();

        }

        private void DrawMatrix()
        {
            if (_m == null) return;
           
            PrintToConsole();
            PrintToHtml();
            PrintToGraphic();
            Refresh();
        }

        private void PrintToGraphic()
        {
            _graphic.SetProperty(TableDrawingSystemConstants.GraphicTableDrawingSystemDocParam, dataGridView1);
            var matrixDrawer = MatrixDrawerBuilder.GetDrawer(Matrix);
            matrixDrawer.SetSystem(_graphic);
            matrixDrawer.Draw();
        }

        private void PrintToConsole()
        {
            MemoryStream s = new MemoryStream();
            _console.SetProperty(TableDrawingSystemConstants.ConsoleTableDrawingSystemStreamParam, s);
            var matrixDrawer = MatrixDrawerBuilder.GetDrawer(Matrix);
            matrixDrawer.SetSystem(_console);
            matrixDrawer.Draw();
            s.Position = 0;
            StreamReader reader = new StreamReader(s);
            textBox2.Text = reader.ReadToEnd();
        }

        private void PrintToHtml()
        {
           
            _html.SetProperty(TableDrawingSystemConstants.HtmlTableDrawingSystemDocParam, webBrowser1);
            var matrixDrawer = MatrixDrawerBuilder.GetDrawer(Matrix);
            matrixDrawer.SetSystem(_html);
            matrixDrawer.Draw();
           
       
        }

        private void sparseMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SparseMatrixComannd(this, 3,3).Execute();
        }

        private void transposeToolStripMenuItem_Click(object sender, EventArgs e)
        {
             new TransposeMatrixComannd(this).Execute();
        }

        private void zeroAfterDiagToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new LowerTriangularMatrixComannd(this).Execute();
        }

        private void minor02ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new MinorMatrixComannd(this, new List<int>() {0, 1}, new List<int>() {0, 1}).Execute();
        }

        private void undoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CommandManager.GetInstance().Undo();
            Refresh();
        }

        public void Clean()
        {
            textBox2.Text = "";
            webBrowser1.DocumentText = String.Empty;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            Refresh();
        }
    }
}
