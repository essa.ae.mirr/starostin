﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    class SparseMatrixDrawer : IMatrixDrawer
    {
        private IMatrix _m;
        private ITableDrawingSystem _system;
        internal SparseMatrixDrawer(IMatrix m)
        {
            _m = m;
        }
        public void SetSystem(ITableDrawingSystem system)
        {
            _system = system;
            system.Init(_m.GetRows(), _m.GetCols());
        }

        public void Draw()
        {
            _system.AddOutsideBorder(false);
            for (int i = 0; i < _m.GetRows(); i++)
            {
                for (int j = 0; j < _m.GetCols(); j++)
                {
                    if (_m.GetValue(i, j) != 0)
                    {
                        _system.AddCellBorder(i, j);
                        _system.AddValue(_m.GetValue(i, j), i, j);
                    }
                    
                }
            }

            _system.DrawResultingTable();
        }
    }
}
