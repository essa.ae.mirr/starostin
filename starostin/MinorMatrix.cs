﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    class MinorMatrix : IMatrix
    {
        private IMatrix _m;
        private readonly List<int> cols;
        private readonly List<int> rows;

        public MinorMatrix(IMatrix m, List<int>cols, List<int> rows)
        {
            _m = m;
            this.cols = cols;
            this.rows = rows;
        }

        public void SetValue(int row, int col, int value)
        {
            _m.SetValue(rows[row], cols[col], value);
        }

        public int GetValue(int row, int col)
        {
            return _m.GetValue(rows[row], cols[col]);
        }

        public int GetRows()
        {
            return rows.Count();
        }

        public int GetCols()
        {
            return cols.Count();
        }

        public MatrixType GeType()
        {
            return _m.GeType();
        }
        
    }
}
