﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Random;

namespace starostin
{
    class SparseMatrix : IMatrix
    {
        private MathNet.Numerics.LinearAlgebra.Double.SparseMatrix _m;
        private int _rows;
        private int _cols;
        private Random _r = new Random();

        public SparseMatrix(int rows, int cols)
        {
            _m = new MathNet.Numerics.LinearAlgebra.Double.SparseMatrix(rows, cols);
        }
        public void SetValue(int row, int col, int value)
        {
            _m[row, col] = value;

        }

        public int GetValue(int row, int col)
        {
            return (int) _m[row, col];
        }

        public int GetRows()
        {
            return _m.RowCount;
        }

        public int GetCols()
        {
            return _m.ColumnCount;
        }

        public MatrixType GeType()
        {
            return MatrixType.Sparse;
        }

        public void GenerateSimple()
        {
            SetValue(_r.Next(2), _r.Next(2), _r.Next(9));
            SetValue(_r.Next(2), _r.Next(2), _r.Next(9));
            SetValue(_r.Next(2), _r.Next(2), _r.Next(9));
        }
    }
}
