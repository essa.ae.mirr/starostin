﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    public enum MatrixType
    {
        Dense,
        Sparse
    }

    public interface IMatrix
    {
        void SetValue(int row, int col, int value);
        int GetValue(int row, int col);
        int GetRows();
        int GetCols();
        MatrixType GeType();
    }
}
