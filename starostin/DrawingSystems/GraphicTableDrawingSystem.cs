﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace starostin
{
    public class GraphicTableDrawingSystem : ITableDrawingSystem
    {
        private DataGridView _grid;
        private int _rows;
        private int _cols;
        List<Tuple<int,int>> _borderedCells = new List<Tuple<int,int>>();
        public void Init(int rows, int cols)
        {
            _rows = rows;
            _cols = cols;
            _grid.Rows.Clear();
            _grid.Columns.Clear();
            for (int i = 0; i < cols; i++)
            {
                _grid.Columns.Add(i.ToString(), String.Empty);
            }
            
            _grid.Rows.Add(rows);
            _grid.RowHeadersVisible = false;
            _grid.BorderStyle = BorderStyle.None;
            _borderedCells.Clear();



        }

        public void AddValue(int value, int colIndex, int rowIndex)
        {
            _grid[colIndex, rowIndex].Value = value;
        }

        public void AddCellBorder(int colIndex, int rowIndex)
        {
            _borderedCells.Add(new Tuple<int, int>(rowIndex, colIndex));
        }

        public void AddOutsideBorder(bool border)
        {
            if (border)
            {
                _grid.BorderStyle = BorderStyle.FixedSingle;
                _grid.AdvancedCellBorderStyle.All = DataGridViewAdvancedCellBorderStyle.Outset;
            }
            else
            {
                _grid.BorderStyle = BorderStyle.None;
                _grid.AdvancedCellBorderStyle.All = DataGridViewAdvancedCellBorderStyle.None;
            }
        }

        public void DrawResultingTable()
        {
            _grid.CellPainting += GridOnCellPainting;
        }

        private void GridOnCellPainting(object sender, DataGridViewCellPaintingEventArgs dataGridViewCellPaintingEventArgs)
        {
           if(_borderedCells.Contains(new Tuple<int, int>(dataGridViewCellPaintingEventArgs.RowIndex, dataGridViewCellPaintingEventArgs.ColumnIndex)))
                dataGridViewCellPaintingEventArgs.Graphics.DrawRectangle(new Pen(Color.Red, 4), dataGridViewCellPaintingEventArgs.CellBounds);
           
        }

        

        public void SetProperty(string name, object value)
        {
            if (name != TableDrawingSystemConstants.GraphicTableDrawingSystemDocParam)
                return;
            else
            {
                _grid =(DataGridView) value;
            }
        }
    }
}