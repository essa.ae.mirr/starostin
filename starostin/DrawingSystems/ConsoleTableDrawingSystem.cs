﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    class ConsoleTableDrawingSystem : ITableDrawingSystem
    {
        private ConsoleCell[,] values;
        private int _rows;
        private int _cols;
        private Stream _textStream = Console.OpenStandardOutput();
        private String _resultString;
        public void Init(int rows, int cols)
        {
           values = new ConsoleCell[rows,cols];
            _rows = rows;
            _cols = cols;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    values[i, j] = new ConsoleCell();
                }
            }
        }

        

        public void AddValue(int value, int colIndex, int rowIndex)
        {
            values[rowIndex,colIndex].SetValue(Convert.ToString(value)[0]);
        }
     

        public void AddCellBorder(int colIndex, int rowIndex)
        {
            values[rowIndex, colIndex].FillBorder();
        }

        public void AddOutsideBorder(bool border)
        {
            if (border)
            {
                for (int i = 0; i < _rows; i++)
                {
                    values[i, 0].FillLeftBorder();
                    values[i, _cols - 1].FillRightBorder();
                }

                for (int j = 0; j < _cols; j++)
                {
                    values[0, j].FillUpperBorder();
                    values[_rows - 1, j].FillBottomBorder();
                }
            }

        }

        public void DrawResultingTable()
        {
           
            StreamWriter sw = new StreamWriter(_textStream);
            
            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _cols; j++)
                {
                    sw.Write(values[i, j].GetRow(0));
                }
                sw.Write(Environment.NewLine);
                for (int j = 0; j < _cols; j++)
                {
                    sw.Write(values[i, j].GetRow(1));
                }
                sw.Write(Environment.NewLine);
                for (int j = 0; j < _cols; j++)
                {
                    sw.Write(values[i, j].GetRow(2));
                }
                sw.Write(Environment.NewLine);
            }
            sw.Flush();
        }

        public void SetProperty(string name, object value)
        {
            if (name != TableDrawingSystemConstants.ConsoleTableDrawingSystemStreamParam)
                return;
            else
            {
                _textStream = (Stream) value;
            }
        }

        class ConsoleCell
        {
            char[][] _cell = new char[3][];

            internal ConsoleCell()
            {
                for (int i = 0; i < 3; i++)
                {
                    _cell[i] = new char[3];
                }

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        _cell[i][j] = ' ';
                    }
                }
            }

            public void FillUpperBorder()
            {
                for (int i = 0; i < 3; i++)
                    _cell[0][i] = '-';
            }

            public void FillBottomBorder()
            {
                for (int i = 0; i < 3; i++)
                    _cell[2][i] = '-';
            }

            public void FillLeftBorder()
            {
                for (int i = 0; i < 3; i++)
                    _cell[i][0] = '|';
            }

            public void FillRightBorder()
            {
                for (int i = 0; i < 3; i++)
                    _cell[i][2] = '|';
            }

            public void FillBorder()
            {
                FillBottomBorder();
                FillUpperBorder();
                FillLeftBorder();
                FillRightBorder();
                
            }
            public String GetRow(int i)
            {
                return new String(_cell[i]);
            }

            public void SetValue(char val)
            {
                _cell[1][1] = val;
            }
        }
    }
}
