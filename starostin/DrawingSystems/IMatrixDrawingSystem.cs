﻿namespace starostin
{
    public interface ITableDrawingSystem
    {
        void Init(int rows, int cols);
        void AddValue(int value, int colIndex, int rowIndex);
        void AddCellBorder(int colIndex, int rowIndex);
        void AddOutsideBorder(bool border);
        void DrawResultingTable();
        void SetProperty(string name, object value);
    }
}