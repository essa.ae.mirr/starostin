﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
   public class TableDrawingSystemConstants
   {
       public static string ConsoleTableDrawingSystemStreamParam = "stream";
       public static string HtmlTableDrawingSystemDocParam = "doc";
       public static string GraphicTableDrawingSystemDocParam = "grid";
    }
}
