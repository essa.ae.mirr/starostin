﻿using System;
using System.Text;
using System.Windows.Forms;

namespace starostin
{
    public class HtmlTableDrawingSystem : ITableDrawingSystem
    {
        private WebBrowser _doc;
        private int _rows;
        private int _cols;
        private String[,] _values;
        private String[,] _cellProperties;
        private const String enableBorder = "style='border:solid 2px #060'";
        private bool isOuterBorderEnabled = false;
        public void Init(int rows, int cols)
        {
            _rows = rows;
            _cols = cols;
            _values = new String[_rows,_cols];
            _cellProperties = new String[_rows, _cols];
            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _cols; j++)
                {
                    _values[i,j] = "";
                    _cellProperties[i, j] = "";
                }
            }

        }

        public void AddValue(int value, int colIndex, int rowIndex)
        {
            _values[rowIndex, colIndex] = value.ToString();
        }

        public void AddCellBorder(int colIndex, int rowIndex)
        {
            _cellProperties[rowIndex, colIndex] = enableBorder;
        }

        public void AddOutsideBorder(bool border)
        {
            isOuterBorderEnabled = border;
        }

        public void DrawResultingTable()
        {
            StringBuilder builder = new StringBuilder("");
            builder.Append("<!DOCTYPE html> <html> <head> <title> Page Title </title > </head><body>" );
            builder.Append("<table" + (isOuterBorderEnabled? " frame='box' border = '2' bordercolor='red'" : "")
                           +">");
            for (int i = 0; i < _rows; i++)
            {
                builder.Append("<tr>");

                for (int j = 0; j < _cols; j++)
                {
                    builder.Append("<td " + _cellProperties[i, j] 
                                   + ">");
                    builder.Append(_values[i, j]);
                    builder.Append("</td>");
                }
                builder.Append("</tr>");
            }

            builder.Append("</table>");
            builder.Append(" </body></html>");
            _doc.DocumentText = builder.ToString();

        }

        public void SetProperty(string name, object value)
        {
            if (name != TableDrawingSystemConstants.HtmlTableDrawingSystemDocParam)
                return;
            else
            {
                _doc =  (WebBrowser) value;
            }
        }
    }
}