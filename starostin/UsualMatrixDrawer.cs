﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starostin
{
    class UsualMatrixDrawer : IMatrixDrawer
    {
        private ITableDrawingSystem _system;
        private IMatrix _m;

        public UsualMatrixDrawer(IMatrix m)
        {
            _m = m;
        }

        public void SetSystem(ITableDrawingSystem system)
        {
            _system = system;
            system.Init(_m.GetRows(), _m.GetCols());
        }

        

        public void Draw()
        {
            _system.AddOutsideBorder(true);
            for (int i = 0; i < _m.GetRows(); i++)
            {
                for (int j = 0; j < _m.GetCols(); j++)
                {
                    _system.AddCellBorder(i,j);
                    _system.AddValue(_m.GetValue(i,j), i,j);
                }
            }

            _system.DrawResultingTable();
        }
    }
}
