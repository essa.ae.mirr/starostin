﻿namespace starostin
{
    public class TransposedMatrix : IMatrix
    {
        private IMatrix _m;
        public TransposedMatrix(IMatrix m)
        {
            _m = m;
        }
        public void SetValue(int row, int col, int value)
        {
            _m.SetValue(col,row,value);
        }

        public int GetValue(int row, int col)
        {
            return _m.GetValue(col, row);
        }

        public int GetRows()
        {
            return _m.GetRows();
        }

        public int GetCols()
        {
            return _m.GetCols();
        }

        public MatrixType GeType()
        {
            return _m.GeType();
        }

       
    }
}